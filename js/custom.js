$('.testimonials-slider').owlCarousel({
  loop: true,
  margin: 0,
  nav: false,
  dots: true,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items: 1
    }
  }
});

wow = new WOW({
  mobile: false, // default
})
wow.init();

var h = jQuery("header");
var body = jQuery("body");
jQuery(window).scroll(function() {
  var windowpos = jQuery(window).scrollTop();
  if (windowpos >= 141) {
    h.addClass("stick ");
    body.addClass("stick ");
  } else {
    h.removeClass("stick ");
    body.removeClass("stick ");
  }
});
